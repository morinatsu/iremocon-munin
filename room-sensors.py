#!/usr/bin/env python
#! -*- coding: utf-8 -*-
"""
Display Info of Sencers in i-remocon
"""

from iremocon import IRemocon


remocon = IRemocon('iremocon.yaml')

# send command
answer = remocon.SendCommand(b'*se\r\n').decode('ascii')

# parse answer
if answer.startswith('se;ok;'):
    print(answer.rstrip('\r\n'))
